function [boxx, outImg, flag, bbox_cnt, max_score, message] = ONE_BOXX3(img, pretrained_detector)
persistent yolov2Obj;

if isempty(pretrained_detector.detector)
    message = "Detector not found";
else
    flag = 0;
    max_score = 0;
    box_count = 0;
%[bboxes, scores] = yolov2Obj.detect(img);
    [bboxes, scores] = detect(pretrained_detector.detector, img);
    if isempty(bboxes)
        outImg = img;
    else
        if length(scores) > 1
           for j = 1:length(scores)
                if j == 1
                    max_score = double(scores(j));
                    box_count = j * 4;
                else
                    if scores(j) > max_score
                        max_score = double(scores(j));
                        box_count = j * 4;
                    end
                end
           end
           flag = 1;
        end
    end
end
% if isempty(yolov2Obj)
%     yolov2Obj = coder.loadDeepLearningNetwork('TEST_2.mat');
% end

boxx = bboxes;
bbox_cnt = box_count;
outImg = img;
message = "Detect successful";
end