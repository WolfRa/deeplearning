import random
import torch
from torch import nn
from torch.nn import functional as F

def softmax(x, temperature=10): # use your temperature
    e_x = torch.exp(x / temperature)
    return e_x / torch.sum(e_x, dim=0)

class Encoder(nn.Module):
    def __init__(self, input_dim, emb_dim, hid_dim, n_layers, dropout, bidirectional):
        super().__init__()
        
        self.input_dim = input_dim
        self.emb_dim = emb_dim
        self.hid_dim = hid_dim
        self.n_layers = n_layers
        self.dropout = dropout
        self.bidirectional = bidirectional
        
        self.embedding = nn.Embedding(input_dim, emb_dim)
        
        self.rnn = nn.LSTM(emb_dim, hid_dim, num_layers=n_layers, dropout=dropout, bidirectional=bidirectional)
        
        self.dropout = nn.Dropout(p=dropout)
        
    def forward(self, src):
        
        #src = [src sent len, batch size]
        
        # Compute an embedding from the src data and apply dropout to it
        embedded = self.dropout(self.embedding(src))
        
        #embedded = [src sent len, batch size, emb dim]
        
        # Compute the RNN output values of the encoder RNN. 
        # outputs, hidden and cell should be initialized here. Refer to nn.LSTM docs ;)
        
        outputs, (hidden, cell) = self.rnn(embedded)
        
        #outputs = [src sent len, batch size, hid dim * n directions]
        #hidden = [n layers * n directions, batch size, hid dim]
        #cell = [n layers * n directions, batch size, hid dim]
        
        #outputs are always from the top hidden layer
        if self.bidirectional:
            # print("hidden1", hidden.shape)
            hidden = hidden.reshape(self.n_layers, 2, -1, self.hid_dim)
            # print("hidden2", hidden.shape)
            hidden = hidden.transpose(1, 2).reshape(self.n_layers, -1, 2 * self.hid_dim)
            # print("hidden3", hidden.shape)
            # hidden = hidden[self.n_layers - 1 : self.n_layers, :, :]
            # print("hidden4", hidden.shape)

            cell = cell.reshape(self.n_layers, 2, -1, self.hid_dim)
            cell = cell.transpose(1, 2).reshape(self.n_layers, -1, 2 * self.hid_dim)
        return outputs, hidden, cell


class Attention(nn.Module):
    def __init__(self, enc_hid_dim, dec_hid_dim):
        super().__init__()
        
        self.enc_hid_dim = enc_hid_dim
        self.dec_hid_dim = dec_hid_dim
        
        self.attn = nn.Linear(enc_hid_dim + dec_hid_dim, enc_hid_dim)
        self.v = nn.Linear(enc_hid_dim, 1)
        
    def forward(self, hidden, encoder_outputs):
        # encoder_outputs = [src sent len, batch size, enc_hid_dim]
        # hidden = [1, batch size, dec_hid_dim]
        # print("     encoder_outputs.shape", encoder_outputs.shape)
        # print("     hidden.shape", hidden.shape)
        hidden = hidden.repeat(len(encoder_outputs), 1, 1)
        # repeat hidden and concatenate it with encoder_outputs

        # print("encoder_outputs.shape", encoder_outputs.shape)
        # print("hidden.shape", hidden.shape)
        attn = torch.cat((encoder_outputs, hidden), 2)
        # calculate energy
        # print("attn.shape", attn.shape)
        energy = torch.tanh(self.attn(attn))

        # get attention, use softmax function which is defined, can change temperature
        temp_attn = self.v(energy)

        attention = softmax(temp_attn)
        return attention
    
    
class DecoderWithAttention(nn.Module):
    def __init__(self, output_dim, emb_dim, enc_hid_dim, dec_hid_dim, dropout, attention):
        super().__init__()

        self.emb_dim = emb_dim
        self.enc_hid_dim = enc_hid_dim
        self.dec_hid_dim = dec_hid_dim
        self.output_dim = output_dim
        self.attention = attention
        
        self.embedding = nn.Embedding()

        self.rnn = nn.GRU(emb_dim + 2 * enc_hid_dim, 2 * dec_hid_dim, dropout=dropout)  # use GRU

        self.out = nn.Linear(emb_dim + 2 * enc_hid_dim + 2 * dec_hid_dim, output_dim)  # linear layer to get next word
        
        self.dropout = nn.Dropout(dropout)

    def forward(self, input, hidden, encoder_outputs):
        # input = [batch size]
        # hidden = [n layers * n directions, batch size, hid dim]

        # n directions in the decoder will both always be 1, therefore:
        # hidden = [n layers, batch size, hid dim]

        input = input.unsqueeze(0)  # because only one word, no words sequence

        # input = [1, batch size]

        embedded = self.dropout(self.embedding(input))

        # embedded = [1, batch size, emb dim]

        # get weighted sum of encoder_outputs
        atts = attention(hidden, encoder_outputs)
        # print("atts.shape: ", atts.shape)
        # print("encoder_outputs.shape: ", encoder_outputs.shape)
        w_t = torch.zeros(1, encoder_outputs.shape[1], encoder_outputs.shape[2])
        w_t = w_t.to(device)
        for i in range(encoder_outputs.shape[0]):
            m = encoder_outputs[i] * atts[i]
            w_t += m
        # concatenate weighted sum and embedded, break through the GRU
        # print("w_t.shape: ", w_t.shape)
        # print("embedded.shape: ", embedded.shape)
        p = torch.cat((embedded, w_t), 2)
        # print("p.shape: ", p.shape)
        # print("hidden.shape: ", hidden.shape)
        output, s_t = self.rnn(p, hidden)
        # print("s_t.shape: ", s_t.shape)
        # print('/n')
        # get predictions
        y_tp = self.out(torch.cat((embedded, w_t, s_t), 2))
        y_tp = y_tp.view(y_tp.shape[1], y_tp.shape[2])
        # prediction = [batch size, output dim]

        return y_tp, s_t, encoder_outputs
        

class Seq2Seq(nn.Module):
    def __init__(self, encoder, decoder, device):
        super().__init__()
        
        self.encoder = encoder
        self.decoder = decoder
        self.device = device
        
        assert encoder.hid_dim == decoder.dec_hid_dim, \
            "Hidden dimensions of encoder and decoder must be equal!"

    def forward(self, src, trg, teacher_forcing_ratio=0.5):
        # src = [src sent len, batch size]
        # trg = [trg sent len, batch size]
        # teacher_forcing_ratio is probability to use teacher forcing
        # e.g. if teacher_forcing_ratio is 0.75 we use ground-truth inputs 75% of the time

        # Again, now batch is the first dimention instead of zero
        batch_size = trg.shape[1]
        trg_len = trg.shape[0]
        trg_vocab_size = self.decoder.output_dim

        # tensor to store decoder outputs
        outputs = torch.zeros(trg_len, batch_size, trg_vocab_size).to(self.device)

        # last hidden state of the encoder is used as the initial hidden state of the decoder
        enc_states, hidden, cell = self.encoder(src)
        # print("enc_states.shape", enc_states.shape)
        # print("hidden.shape", hidden.shape)
        # hidden = (hidden[-1]).view(1,hidden.shape[1], hidden.shape[2])
        hidden = hidden[N_LAYERS - 1: N_LAYERS, :, :]
        # print("hidden", hidden)

        # first input to the decoder is the <sos> tokens
        input = trg[0, :]

        for t in range(1, trg_len):
            # print("input.shape: ", input.shape)

            output, hidden, encoder_outputs = self.decoder(input, hidden, enc_states)
            # print("output.shape: ", output.shape)
            # print("hidden.shape: ", hidden.shape)
            outputs[t] = output
            # decide if we are going to use teacher forcing or not
            teacher_force = random.random() < teacher_forcing_ratio
            # get the highest predicted token from our predictions
            top1 = output.argmax(-1)
            # if teacher forcing, use actual next token as next input
            # if not, use predicted token
            input = trg[t] if teacher_force else top1

        return outputs
